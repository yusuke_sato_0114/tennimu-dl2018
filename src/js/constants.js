import keyMirror from 'keyMirror'

export const ActionTypes = keyMirror({
  WINDOW_SIZE_SET: null,
  MODAL_SHOW: null,
  MODAL_HIDE: null,
  PHONE_INCOMING: null,
  PHONE_LOADING: null,
  PHONE_SPEAKING: null,
  PHONE_STOP: null,
  DATE_GET: null,
  DATE_SET: null,
  TODAY_JSON_GET: null,
  SPECIAL_JSON_GET: null,
  SPECIAL_CAST: null,
  RANDOM_CAST: null,
  DEVICE_SET: null,
  HISTORY_SHOW: null,
  HISTORY_HIDE: null,
  SCHEDULE_SET: null,
  STATUS_SET: null
})