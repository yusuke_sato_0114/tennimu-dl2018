import React, { Component } from 'react'

export default class Footer extends Component {
  render() {
    return (
      <footer className="footer">
        <div className="inner">
          <div className="copyrights">
            <div className="copyright copyright--tennimu">
              <a className="copyright--tennimu__link" href="https://www.tennimu.com/"></a>
              &copy;&nbsp;許斐 剛／集英社・ＮＡＳ・新テニスの王子様プロジェクト<br/>
              &copy;&nbsp;許斐 剛／集英社・テニミュ製作委員会
            </div>
            <div className="copyright copyright--jasrac">
              JASRAC許諾番号<br/>
              9010506018Y45040
            </div>
          </div>
        </div>
      </footer>
    )
  }
}
