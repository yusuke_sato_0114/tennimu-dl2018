import React, { Component } from 'react'
import { findDOMNode } from 'react-dom'
import classNames from 'classnames'

export default class Header extends Component {
  render() {
    return (
      <header className="header">
        <div className="top-visual">
          <img className="top-visual__image top-visual__image--pc" src="img/pc/top.jpg" />
          <img className="top-visual__image top-visual__image--sp" src="img/sp/top.jpg" />
        </div>
      </header>
    )
  }
}
