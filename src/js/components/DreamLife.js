import React, { Component } from 'react'
import classNames from 'classnames'

export default class DreamLife extends Component {
  playCall() {
    const { call, actions, screen } = this.props

    call.volume = 1;
    call.play();

    if (screen.schedule === 'daily') {
      actions.modalShow({
        modalType: 'daily'
      });
    } else if (screen.schedule === 'history') {
      actions.modalShow({
        modalType: 'random'
      })
    }

    gtag('event', 'click', {
      'event_category': 'sp-btn',
      'event_action': 'click',
      'event_label': 'cv',
      'value': 100
    });
    // ga('send','event','sp-btn','click','cv',100,{'nonInteraction':1})
  }

  showHistory() {
    this.props.actions.historyShow();
    window.scrollTo(0, 0)
  }

  render() {
    const { screen } = this.props
    const phoneClass = classNames('dreamlife-phone', {
      'dreamlife-phone--incoming': screen.phoneState === 0
    })
    const noteElements = {pc: [], sp: []};
    let descriptionElements = null
    let historyButtonElement = null

    if (screen.today.date < screen.dreamlifeEndAt) {
      descriptionElements = (
        <div className="dreamlife-header__descriptions">
          <div className="dreamlife-header__description">
            <div className="dreamlife-header__description-heading">Dream Life 2 - ドリームライフ2 -</div>
            毎日日替わりで、Dream Live 2018出演キャストの<br/>
            ビデオ通話風の動画を観ることができます。<br/>
            期間限定の特別動画をぜひお楽しみください！<br/>
            たくさん電話を取るとレア動画が観れるかも!?
          </div>
          <div className="dreamlife-header__description">
            <div className="dreamlife-header__description-heading">遊び方</div>
            1　電話をタップ or クリックして着信画面を表示<br/>
            2　緑色の通話ボタンを押すと動画が再生されます<br/>
            3　赤い通話終了ボタンを押すと終了します
          </div>
        </div>
      )

      noteElements.pc.push(
        <div className="dreamlife-notes__item" key="1">
          ・毎日正午12時に更新されます。
        </div>
      )
      
      noteElements.pc.push(
        <div className="dreamlife-notes__item" key="2">
          ・次の更新時間までは何回でも動画を見ることができます。
        </div>
      )

      noteElements.sp.push(
        <div className="dreamlife-notes__item" key="1">
          ・毎日正午12時に更新されます。
        </div>
      )

      noteElements.sp.push(
        <div className="dreamlife-notes__item" key="2">
          ・次の更新時間までは何回でも動画を見ることが<br/>
          できます。
        </div>
      )
    } else {
      descriptionElements = (
        <div className="dreamlife-header__descriptions">
          <div className="dreamlife-header__description">
            <div className="dreamlife-header__description-heading">Dream Life 2 - ドリームライフ2 -</div>
            Dream Live 2018の開催を記念して、<br/>
            日替わり公開していたビデオ通話風動画を<br/>
            振り返ることができる、着信履歴機能が登場！<br/>
          </div>
          <div className="dreamlife-header__description">
            また、電話のボタンからは過去の動画がランダム<br/>
            に再生可能！レア動画も観ることができます。<br/>
            <div className="dreamlife-header__description--note">※着信履歴からレア動画を観ることはできません。</div>
          </div>
        </div>
      )

      historyButtonElement = (
        <div className="dreamlife-history-btn" onClick={this.showHistory.bind(this)}></div>
      )
    }

    return (
      <section id="dreamlife" className="dreamlife">
        <div className="inner">
          <div className="dreamlife-header">
            <div className="dreamlife-header__title"></div>
            { descriptionElements }
          </div>
          <div className={ phoneClass } onClick={this.playCall.bind(this)}></div>
          { historyButtonElement }
          <div className="dreamlife-notes">
            <div className="dreamlife-notes__heading">注意事項</div>
            <div className="dreamlife-notes__list dreamlife-notes__list--pc">
              { noteElements.pc }
              <div className="dreamlife-notes__item">
                ・本企画は動画を再生するコンテンツのため、<br/>
                <span className="dreamlife-notes__text dreamlife-notes__text--emphasis">
                  パケット定額サービスでご利用頂くか、WiFiに接続してご利用頂くことを推奨致します。
                </span>
              </div>
              <div className="dreamlife-notes__item">
                ・ビデオ通話風の動画のため、映像がぶれる可能性がございます。ご了承ください。
              </div>
              <div className="dreamlife-notes__item">
                ・推奨ブラウザは次の通りです。Internet Explorer / Safari / Chrome<br/>
                うまく動かない場合は最新版にアップデートされているかご確認ください。
              </div>
              <div className="dreamlife-notes__item">
                ・コンテンツの無断転載はご遠慮ください。
              </div>
            </div>
            <div className="dreamlife-notes__list dreamlife-notes__list--sp">
              { noteElements.sp }
              <div className="dreamlife-notes__item">
                ・本企画は動画を再生するコンテンツのため、<br/>
                <span className="dreamlife-notes__text dreamlife-notes__text--emphasis">
                  パケット定額サービスでご利用頂くか、<br/>
                  Wi-Fiに接続してご利用頂くことを推奨致します。
                </span>
              </div>
              <div className="dreamlife-notes__item">
                ・ビデオ通話風の動画のため、映像がぶれる可能性が<br/>
                ございます。ご了承ください。
              </div>
              <div className="dreamlife-notes__item">
                ・推奨ブラウザは次の通りです。<br/>
                Internet Explorer / Safari / Chrome<br/>
                うまく動かない場合は最新版にアップデート<br/>
                されているかご確認ください。
              </div>
              <div className="dreamlife-notes__item">
                ・コンテンツの無断転載はご遠慮ください。
              </div>
            </div>
          </div>
        </div>
      </section>
    )
  }
} 
