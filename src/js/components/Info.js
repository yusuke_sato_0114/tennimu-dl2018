import React, { Component } from 'react'

export default class Info extends Component {
  render() {
    return (
      <section id="info" className="info">
        <div className="inner">
          <div className="info__title"></div>
          <a className="info__btn" href="https://www.tennimu.com/3rd_dl2018/ticket.html" target="_blank">詳しくはこちら</a>
          <div className="info__stages">
            <div className="stage">
              <div className="stage__place">
                <div className="stage__city-name">神戸</div>
                <div className="stage__hall-name">神戸ワールド記念ホール</div>
              </div>
              <div className="stage__schedule">2018年5月5日（土･祝）〜6日（日）</div>
              <div className="stage__table stage__table--kobe"></div>
              <div className="stage__tickets">
                <div className="stage__ticket">
                  <span className="stage__ticket-shop">●ｅ＋（イープラス）</span><br/>
                  <a href="http://eplus.jp/tennimu/" target="_blank">http://eplus.jp/tennimu/</a>（PC・携帯）<br/>
                  ファミリーマート店内Famiポート
                </div>
                <div className="stage__ticket">
                  <span className="stage__ticket-shop">●チケットぴあ</span><br/>
                  <a href="http://w.pia.jp/t/tennimu/" target="_blank">http://w.pia.jp/t/tennimu/</a>（PC・携帯）<br/>
                  TEL：0570-02-9950<br/>
                  （発売日特電：4/8 10:00～18:00）<br/>
                  TEL：0570-02-9999（Pコード：483-979）<br/>
                  セブン-イレブン、サークルＫ・サンクス、<br/>
                  チケットぴあ店舗
                </div>
                <div className="stage__ticket">
                  <span className="stage__ticket-shop">●ローソンチケット</span><br/>
                  <a href="http://l-tike.com/tennimu/" target="_blank">http://l-tike.com/tennimu/</a>（PC・携帯）<br/>
                  TEL：0570-084-632<br/>
                  （発売日特電：4/8 10:00～18:00）<br/>
                  TEL：0570-084-005（Lコード：53148）<br/>
                  TEL：0570-000-407（オペレーター対応）<br/>
                  ローソン、ミニストップ店内Loppi
                </div>
              </div>
            </div>
            <div className="stage">
              <div className="stage__place">
                <div className="stage__city-name">横浜</div>
                <div className="stage__hall-name">横浜アリーナ</div>
              </div>
              <div className="stage__schedule">2018年5月19日（土）〜20日（日）</div>
              <div className="stage__table stage__table--yokohama"></div>
              <div className="stage__tickets">
                <div className="stage__ticket">
                  <span className="stage__ticket-shop">●ｅ＋（イープラス）</span><br/>
                  <a href="http://eplus.jp/tennimu/" target="_blank">http://eplus.jp/tennimu/</a>（PC・携帯）<br/>
                  ファミリーマート店内Famiポート
                </div>
                <div className="stage__ticket">
                  <span className="stage__ticket-shop">●チケットぴあ</span><br/>
                  <a href="http://w.pia.jp/t/tennimu/" target="_blank">http://w.pia.jp/t/tennimu/</a>（PC・携帯）<br/>
                  TEL：0570-02-9960<br/>
                  （発売日特電：4/8 10:00～18:00）<br/>
                  TEL：0570-02-9999（Pコード：483-980）<br/>
                  セブン-イレブン、サークルＫ・サンクス、<br/>
                  チケットぴあ店舗
                </div>
                <div className="stage__ticket">
                  <span className="stage__ticket-shop">●ローソンチケット</span><br/>
                  <a href="http://l-tike.com/tennimu/" target="_blank">http://l-tike.com/tennimu/</a>（PC・携帯）<br/>
                  TEL：0570-084-633<br/>
                  （発売日特電：4/8 10:00～18:00）<br/>
                  TEL：0570-084-003（Lコード：32777）<br/>
                  TEL：0570-000-407（オペレーター対応）<br/>
                  ローソン、ミニストップ店内Loppi
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    )
  }
}
