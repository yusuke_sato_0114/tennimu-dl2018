import React, { Component } from 'react'
import classNames from 'classnames'
import { getQuery, getTweetText } from '../util'

export default class Modal extends Component {
  onClickOverlay() {
    if (this.props.screen.device === 'pc') {
      this.props.actions.modalHide()
    }
  }

  componentWillReceiveProps(nextProps) {
    const { actions, screen, call } = this.props
    const { modal, video, phone } = this.refs

    if(screen.displayModal !== nextProps.screen.displayModal) {
      if(nextProps.screen.displayModal) { // 非表示から表示
        // 着信状態に遷移
        actions.phoneIncoming()
        modal.style.opacity = 1
        
        if (screen.device === 'sp') {
          // 先に背景を真っ暗にする
          modal.style.opacity = 1
          // スクロール位置を取っておく
          this.scrollTop = window.scrollY

          let timer = setInterval(() => {
            // 背景が完全に真っ黒になるまで待機
            if (modal.style.opacity === '1') {
              // 保険で200msおいて
              setTimeout(() => {
                // 画面の高さを100%で固定
                document.querySelector('html').style.height = '100%'
                document.querySelector('html').style.overflow = 'hidden'
                document.querySelector('body').style.height = '100%'
                document.querySelector('body').style.overflow = 'hidden'
                // 電話DOMを表示
                phone.style.opacity = 1
                clearInterval(timer)
              }, 200)
            }
          }, 50)
        }
      } else { // 表示から非表示
        video.pause()
        if (!isNaN(video.duration)) {
          video.currentTime = 0
        }

        call.pause()
        if (!isNaN(call.duration)) {
          call.currentTime = 0
        }

        if (screen.device === 'sp') {
          // html, bodyの高さを戻し
          document.querySelector('html').style.height = 'auto'
          document.querySelector('html').style.overflow = 'auto'
          document.querySelector('body').style.height = 'auto'
          document.querySelector('body').style.overflow = 'auto'
          // 保存しておいたスクロール位置に復帰
          window.scrollTo(0, this.scrollTop)
          // 電話DOMを消す
          phone.style.opacity = 0
        }

        // モーダルを閉じ、完了したら着信状態に遷移
        modal.style.opacity = 0
        setTimeout(() => {
          actions.phoneIncoming()

          if (screen.modalType === 'daily') {
            actions.castSpecial()
          } else if (screen.modalType === 'random') {
            actions.castRandom()
          }

          video.src = ''
        }, 300)
      }
    }
  }

  componentDidMount() {
    this.playing = false;

    setInterval(() => {
      if(this.refs.video) {
        // 動画の再生状態を監視
        if(this.props.screen.displayModal && this.playing && this.refs.video.paused) {
          // 動画が再生中から再生完了に遷移したら終話状態に遷移
          this.props.actions.phoneStop();
        }

        this.playing = !this.refs.video.paused
      }
    }, 100);
  }

  playVideo() {
    const { video } = this.refs
    const { actions, call } = this.props

    const timer = setInterval(() => {
      // console.log(video.readyState)
      if (video.readyState === 4) {
        clearInterval(timer)
        actions.phoneSpeaking()
      }
    }, 50)

    // onClickで非同期処理を挟まず直接video.playしないとiOS Safari制限にかかる
    video.volume = 1
    video.src = video.dataset.src
    video.play()
    
    call.pause();
    actions.phoneLoading()
    gtag('event', 'click', {
      'event_category': 'movie',
      'event_action': 'click',
      'event_label': 'cv',
      'value': 100
    });
    // ga('send','event','movie','click','cv',100,{'nonInteraction':1})
  }

  stopVideo() {
    const { video } = this.refs
    const { actions } = this.props

    video.pause();
    actions.phoneStop();
  }

  // その日の担当と引かれたレア動画が同じかどうか
  isDuplicate() {
    const { screen } = this.props

    if(screen.special.selected.role === '' && screen.role === '') return false;
    return screen.special.selected.role === screen.role;
  }

  render() {
    const { screen, actions } = this.props

    if (screen.schedule === 'thanks') return null;

    const phoneState = [
      '着信中...', '', '', '通話終了'
    ][screen.phoneState]

    // configure modal video, role, name
    let filename = ''
    let role = ''
    let name = ''
    let tweetText = ''

    if (screen.modalType === 'daily') {
      // レア動画と日替わりが重複してたら通常の日替わり、重複してなかったらレア動画
      filename = (screen.special.selected.filename !== '' && !this.isDuplicate()) ?
        `special/${screen.special.selected.filename}` :
        screen.filename

      role = screen.special.selected.role !== '' ?
        screen.special.selected.role :
        screen.role

      name = screen.special.selected.name !== '' ?
        screen.special.selected.name :
        screen.name

      tweetText = (screen.special.selected.role !== '' && !this.isDuplicate()) ?
        getTweetText.dailySpecial(screen.special.selected) :
        getTweetText.daily(screen)
    } else if (screen.modalType === 'history') {
      filename = screen.history.selected.filename
      role = screen.history.selected.role
      name = screen.history.selected.name
      tweetText = getTweetText.history(screen.history.selected)
    } else if (screen.modalType === 'random') {
      // レア動画を引いたらレア動画、そうでなければ通常

      filename = screen.special.selected.filename !== '' ?
        `special/${screen.special.selected.filename}` :
        screen.filename

      role = screen.special.selected.role !== '' ?
        screen.special.selected.role :
        screen.role

      name = screen.special.selected.name !== '' ?
        screen.special.selected.name :
        screen.name

      tweetText = screen.special.selected.role !== '' ?
        getTweetText.randomSpecial(screen.special.selected) :
        getTweetText.random(screen)
    }

    const modalPath = filename === '' ?
      '' :
      `./img/daily/phone/${filename}.jpg${getQuery()}`

    let videoPath = ''

    if (filename !== '') {
      videoPath = screen.isDebug ?
        `./video/${filename}.mp4` :
        `http://d2hngufs1pm2tr.cloudfront.net/${filename}.mp4`
    }

    // action button elements
    const buttonElements = [
      <div className="phone__action" key="close">
        <p className="phone__action-btn phone__action-btn--close" onClick={actions.modalHide}></p>
        <p className="phone__action-label">サイトに戻る</p>
      </div>
    ]

    if(screen.phoneState === 0) {
      buttonElements.push(
        <div className="phone__action" key="start">
          <p className="phone__action-btn phone__action-btn--start" onClick={this.playVideo.bind(this)}></p>
          <p className="phone__action-label">動画再生</p>
        </div>
      );
    }

    // classNames
    const modalClass = classNames('modal', {
      'modal--display': screen.displayModal
    })

    const videoClass = classNames('video', {
      'video--display': screen.phoneState === 2
    })

    const actionsClass = classNames('phone__actions', {
      'phone__actions--calling': screen.phoneState === 0,
      'phone__actions--ended': screen.phoneState == 3
    })

    const loadingClass = classNames('loading', {
      'loading--display': screen.phoneState === 1
    })

    // tweet configure
    const encodedURI = encodeURIComponent('http://dl2018.tennimu.com')
    const encodedTweetText = encodeURIComponent(tweetText)
    const encodedHashTags = encodeURIComponent('ドリライ2018,テニミュ')

    // configure style when device is SP
    const phoneStyle = {
      visibility: screen.phoneState === 2 || screen.phoneState === 1 ? 'hidden' : 'visible'
    }

    if (screen.device === 'sp') {
      phoneStyle.width = `${screen.windowSize.height * (9 / 16)}px`
    }

    const imageStyle = screen.device === 'sp' ?
      {
        height: `${screen.windowSize.height * (9 / 16) * 0.72}px`
      } :
      {}

    return (
      <div className={modalClass} ref="modal">
        <div className="modal__overlay" onClick={this.onClickOverlay.bind(this)}></div>
        <div className="phone" ref="phone">
          <div className="phone__wrapper" style={phoneStyle}>
            <div className="phone__image" style={imageStyle}>
              <img src={modalPath} />
            </div>
            <div className="phone__contents">
              <p className="phone__role">{role}</p>
              <p className="phone__name">{name}</p>
              <p className="phone__state">{phoneState}</p>
              <div className={actionsClass}>{buttonElements}</div>
            </div>
            <a
              className="phone__btn--tweet"
              href={`http://twitter.com/intent/tweet?url=${encodedURI}&text=${encodedTweetText}&hashtags=${encodedHashTags}`}
              onClick={() => gtag('event', 'click', {'event_category': 'share','event_action': 'click','event_label': 'cv','value': 100})}
              target="_blank"
            >シェアツイートする</a>
          </div>
          <div className={videoClass}>
            <video className="video__element" ref="video" playsInline data-src={videoPath} />
            <div className="video__btn" onClick={this.stopVideo.bind(this)}>通話終了</div>
          </div>
          <div className={loadingClass}></div>
        </div>
      </div>
    )
  }
} 
