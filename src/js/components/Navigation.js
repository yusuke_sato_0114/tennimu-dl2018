import React, { Component } from 'react'
import $ from 'jquery'
import classNames from 'classnames'

export default class Navigation extends Component {
  smoothScroll(e) {
    const href = e.target.getAttribute('href')
    const { screen, actions } = this.props

    if (!href) return

    if (screen.history.display) {
      actions.historyHide();
    } else {
      e.preventDefault();
      $('html, body').animate({
        scrollTop: $(href).offset().top
      }, 500)
    }
  }

  render() {
    const { screen } = this.props
    const encodedURI = encodeURIComponent('http://dl2018.tennimu.com')
    const encodedTweetText = encodeURIComponent('ミュージカル『テニスの王子様』15周年記念コンサート Dream Live 2018の特設サイトを公開中！ 日替わり特別企画 ＃ドリームライフ2 では、出演キャストのビデオ通話風動画をぜひお楽しみください！')
    const encodedHashTags = encodeURIComponent('ドリライ2018,テニミュ')
    const countdownTextStyle = {}
    let countdownPath
    let countdownClass

    if (screen.today.date < screen.liveStartAt) {
      countdownPath = screen.filename === '' ?
        '' :
        `./img/daily/countdown/${screen.filename}.png`
      countdownClass = classNames('countdown')
    } else if (screen.today.date < screen.lastDate) {
      countdownPath = './img/daily/countdown/during_stage.png'
      countdownClass = classNames('countdown', 'countdown--during-stage')
    } else {
      countdownPath = './img/daily/countdown/thanks.png'
      countdownClass = classNames('countdown', 'countdown--thanks')
    }

    const countdownElements = screen.history.display ? 
      null : (
        <div className={countdownClass}>
          <img src={countdownPath} />
        </div>
      )

    return (
      <div className="navigation">
        <div className="navigation__wrapper">
          <div className="inner">
            <nav className="navigation__links">
              <a
                className="navigation__link navigation__link--about"
                href="#about"
                onClick={this.smoothScroll.bind(this)}
              ></a>
              <a
                className="navigation__link navigation__link--info"
                href="#info"
                onClick={this.smoothScroll.bind(this)}
              ></a>
              <a
                className="navigation__link navigation__link--dreamlife"
                href="#dreamlife"
                onClick={this.smoothScroll.bind(this)}
              ></a>
            </nav>
            <div className="navigation__shares">
              <a
                className="navigation__share navigation__share--twitter"
                href={`https://twitter.com/share?text=${encodedTweetText}&url=${encodedURI}&hashtags=${encodedHashTags}`}
                target="_blank"
              ></a>
              <a
                className="navigation__share navigation__share--facebook"
                href={`http://www.facebook.com/sharer.php?u=${encodedURI}`}
                target="_blank"
              ></a>
            </div>
          </div>
        </div>
        { countdownElements }
      </div>
    )
  }
}
