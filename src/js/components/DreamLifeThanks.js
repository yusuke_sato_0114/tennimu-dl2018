import React, { Component } from 'react'

export default class DreamLifeThanks extends Component {
  render() {
    const { screen } = this.props
    let descriptionElement

    if (screen.device === 'pc') {
      descriptionElement = (
        <div className="dreamlife-thanks__description">
          <div className="dreamlife-thanks__description-heading">Dream Life 2 終了のお知らせ</div>
          2018年 5月 21日 12:00をもちまして本サービスを終了いたしました。<br/>
          Dream Life 2を遊んでいただき、誠にありがとうございました！<br/><br/>
          今後ともミュージカル『テニスの王子様』をよろしくお願いします。
        </div>
      )
    } else {
      descriptionElement = (
        <div className="dreamlife-thanks__description">
          <div className="dreamlife-thanks__description-heading">Dream Life 2 終了のお知らせ</div>
          2018年 5月 21日 12:00をもちまして<br/>
          本サービスを終了いたしました。<br/>
          Dream Life 2を遊んでいただき、<br/>
          誠にありがとうございました！<br/><br/>
          今後ともミュージカル『テニスの王子様』を<br/>
          よろしくお願いします。
        </div>
      )
    }

    return (
      <section id="dreamlife" className="dreamlife-thanks">
        <div className="inner">
          <div className="dreamlife-thanks__title"></div>
          {descriptionElement}
        </div>
      </section>
    )
  }
} 
