import React, { Component } from 'react'
import classNames from 'classnames'
import { getQuery } from '../util'

export default class HistoryPage extends Component {
  playCall(history) {
    const { call, actions } = this.props

    call.volume = 1;
    call.play();
    actions.modalShow({
      modalType: 'history',
      history
    });
    gtag('event', 'click', {
      'event_category': 'rrk',
      'event_action': 'click',
      'event_label': 'cv',
      'value': 100
    });
    // ga('send','event','sp-btn','click','cv',100,{'nonInteraction':1})
  }

  hideHistory() {
    this.props.actions.historyHide();
    window.scrollTo(0,0);
  }

  render() {
    const { screen } = this.props

    if (!screen.history.display) return null

    const historyElements = screen.history.list.map((history, i) => {
      if(history.role === '青学１年トリオ') {
        return (
          <div className="history-item" onClick={this.playCall.bind(this, history)} key={i}>
            {`${history.date} ${history.role}`}
          </div>
        );
      }

      return (
        <div className="history-item" onClick={this.playCall.bind(this, history)} key={i}>
          {`${history.date} ${history.role} ${history.name}`}
        </div>
      );
    });

    return (
      <div className="history">
        <div className="inner">
          <div className="history-logo"></div>
          <div className="history-title"></div>
          <div className="history-description history-description--pc">タップ or クリックで、過去のボイスが再生できます。</div>
          <div className="history-description history-description--sp">
            タップ or クリックで、<br/>
            過去のボイスが再生できます。
          </div>
          <div className="history-list">{ historyElements }</div>
          <div className="history-btn" onClick={this.hideHistory.bind(this)}>TOPに戻る</div>
        </div>
      </div>
    )
  }
}