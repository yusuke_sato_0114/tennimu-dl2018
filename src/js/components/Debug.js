import React, { Component } from 'react'

export default class Debug extends Component {
  changeDate() {
    location.href = `?year=${this.props.screen.today.year}&month=${this.zero_padding(this.props.screen.today.month)}&day=${this.zero_padding(this.props.screen.today.day)}`;
  }
  zero_padding(num) {
    return ('0' + num.toString()).slice(-2);
  }
  setDate(e) {
    this.props.actions.setDate({
      name: e.target.name,
      value: e.target.value
    });
  }
  render() {
    return (
      <div style={{position:'fixed',zIndex:'9999',bottom:'50px',right:'50px',padding:'10px',backgroundColor:'rgba(255,255,255,0.7)',color:'#000'}}>
        <input type="text" onChange={this.setDate.bind(this)} value={this.props.screen.today.year} name="year" style={{display:'inline-block',width:'30px',border:'1px solid #000'}} />
        <span style={{fontSize:'12px'}}>年</span>
        <input type="text" onChange={this.setDate.bind(this)} value={this.props.screen.today.month} name="month" style={{display:'inline-block',width:'30px',border:'1px solid #000'}} />
        <span style={{fontSize:'12px'}}>月</span>
        <input type="text" onChange={this.setDate.bind(this)} value={this.props.screen.today.day} name="day" style={{display:'inline-block',width:'30px',border:'1px solid #000'}} />
        <span style={{fontSize:'12px'}}>日</span>
        <button onClick={this.changeDate.bind(this)}>へ移動</button>
      </div>
    )
  }
}
