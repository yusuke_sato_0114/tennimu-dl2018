import React, { Component } from 'react'
import About from './About'
import Info from './Info'
import DreamLife from './DreamLife'
import DreamLifeThanks from './DreamLifeThanks'
import classNames from 'classnames'
import { getQuery } from '../util'

export default class NormalPage extends Component {
  render() {
    const { screen } = this.props

    if (screen.history.display) return null

    const dlElement = screen.schedule === 'thanks' ?
      <DreamLifeThanks {...this.props}/> :
      <DreamLife {...this.props} call={this.props.call} />

    return (
      <div className="page--normal">
        <About {...this.props} />
        <Info />
        {dlElement}
      </div>
    )
  }
}