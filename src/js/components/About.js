import React, { Component } from 'react'

export default class About extends Component {
  render() {
    const { screen } = this.props
    let homeiconTextElement

    if (screen.schedule !== 'thanks') {
      if (screen.device === 'pc') {
        homeiconTextElement = (
          <p>
            また、スマートフォンユーザーの方は、日替わりのブックマークアイコンをホーム画面設定することができます！<br/>
            ●SafariまたはAndroidのChromeで、メニューから「ホーム画面に追加」で設定できます。<br/>
            <span>
              ※ホーム画面のブックマークアイコンからサイトを開くと、その日のアイコンに上書きされてしまうのでご注意下さい。
            </span>
          </p>
        )
      } else {
        homeiconTextElement = (
          <p>
            また、スマートフォンユーザーの方は、<br/>
            日替わりのブックマークアイコンを<br/>
            ホーム画面設定することができます！<br/>
            ●SafariまたはAndroidのChromeで、<br/>
            メニューから「ホーム画面に追加」で設定できます。<br/>
            <span>
              ※ホーム画面のブックマークアイコンからサイトを開くと、<br/>
            その日のアイコンに上書きされてしまうのでご注意下さい。
            </span>
          </p>
        )
      }
    }

    return (
      <section id="about" className="about">
        <div className="inner">
          <div className="about__title"></div>
          <div className="about__description about__description--pc">
            <p>
              ミュージカル『テニスの王子様』15周年記念コンサート Dream Live 2018が、2018年5月5日から開催いたします！<br/>
              主役校である青学をはじめ、これまでに熱い戦いを繰り広げてきた六角、立海、比嘉の4校が勢ぞろい。<br/>
              総勢34人が集結し、会場が一体となるライブパフォーマンスをお届けいたします！
            </p>
            <p>
              この特設サイトでは、日替わりでDream Live 2018に出演するキャストたちのビデオ通話風動画を観ることができる<br/>
              「Dream Life 2」を開催中です！ この期間だけの特別動画をぜひお楽しみください！
            </p>
            <p>
              Dream Life2公開期間　2018/04/04 12:00　〜　2018/05/21 11:59
            </p>
            {homeiconTextElement}
          </div>
          <div className="about__description about__description--sp">
            <p>
              ミュージカル『テニスの王子様』<br/>
              15周年記念コンサート Dream Live 2018が、<br/>
              2018年5月5日から開催いたします！<br/>
              主役校である青学をはじめ、<br/>
              これまでに熱い戦いを繰り広げてきた<br/>
              六角、立海、比嘉の4校が勢ぞろい。<br/>
              総勢34人が集結し、会場が一体となる<br/>
              ライブパフォーマンスをお届けいたします！<br/>
            </p>
            <p>
              この特設サイトでは、<br/>
              日替わりでDream Live 2018に出演する<br/>
              キャストたちのビデオ通話風動画を観ることが<br/>
              できる「Dream Life 2」を開催中です！<br/>
              この期間だけの特別動画をぜひお楽しみください！
            </p>
            <p>
              Dream Life2公開期間<br/>
              2018/04/04 12:00　〜　2018/05/21 11:59
            </p>
            {homeiconTextElement}
          </div>
          <div className="about__pv">
            <iframe src="https://www.youtube.com/embed/Tw7VaeXbPo4?rel=0" frameBorder="0" allowFullScreen></iframe>
          </div>
        </div>
      </section>
    )
  }
}
