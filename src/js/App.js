import { connect } from 'react-redux'
import React, { Component } from 'react'
import Header from './components/Header'
import Navigation from './components/Navigation'
import NormalPage from './components/NormalPage'
import HistoryPage from './components/HistoryPage'
import Footer from './components/Footer'
import Modal from './components/Modal'
import Debug from './components/Debug'
import { bindActionCreators } from 'redux'
import actions from './actions'
import CryptoJS from 'crypto-js'
import classNames from 'classnames'
import { getQuery } from './util'

class App extends Component {
  getParsedQuery() {
    const params = {}
    const qs = location.search.slice(1).split('&').filter(q => {
      return q.split('=').length === 2;
    }).forEach(q => {
      const keyValueArr = q.split('=');
      params[keyValueArr[0]] = keyValueArr[1];
    });

    return params;
  }

  componentWillMount() {
    const ua = navigator.userAgent.toLowerCase()
    const device =
      (!/ipad/.test(ua) && /ip/.test(ua)) ||
      (/android/.test(ua) && /mobile/.test(ua)) ?
      'sp' : 'pc'
    const { screen, actions } = this.props

    actions.setDevice({ device });

    if(device === 'pc') {
      document
        .querySelector('.js-viewport')
        .setAttribute('content', 'width=1100, user-scalable=yes')
    }

    actions.setWindowSize({
      width: window.innerWidth,
      height: window.innerHeight
    });

    window.addEventListener('resize', () => {
      actions.setWindowSize({
        width: window.innerWidth,
        height: window.innerHeight
      });
    });

    if(screen.isDebug && Object.keys(this.getParsedQuery()).length === 3) {
      actions.getDate(this.getParsedQuery());
    } else {
      actions.getDate();
    }
  }

  componentDidMount() {
    setInterval(() => {
      const scrollY = window.pageYOffset

      this.refs.fixed_bg_left.style.backgroundPosition = `0 -${scrollY}px`
      this.refs.fixed_bg_right.style.backgroundPosition = `0 -${scrollY}px`
    }, 50)
  }

  componentWillReceiveProps(nextProps) {
    const { screen, actions } = this.props
    const nextScreen = nextProps.screen

    if(screen.status === 'initial' && nextScreen.status === 'dateLoaded') {
      const today = nextScreen.today
      let encrypted

      if (today.date < nextScreen.dreamlifeEndAt) {
        encrypted = CryptoJS.HmacSHA1(today.month + today.day,nextScreen.cryptoKey)
        actions.setSchedule({
          schedule: 'daily'
        })
        actions.getTodayJSON(encrypted);
      } else if (today.date < nextScreen.lastDate) {
        encrypted = CryptoJS.HmacSHA1('all',nextScreen.cryptoKey)
        actions.setSchedule({
          schedule: 'history'
        })
        actions.getTodayJSON(encrypted);
      } else {
        actions.setSchedule({
          schedule: 'thanks'
        })
        actions.setStatus({
          status: 'allLoaded'
        })
      }

      console.log(`Today: ${today.date}`);
    }

    if (screen.status === 'dateLoaded' && nextProps.screen.status === 'dailyJSONLoaded') {
      let filename

      actions.getSpecialJSON();

      if (screen.schedule === 'daily') {
        filename = nextScreen.filename
      } else if (screen.schedule === 'history' || screen.schedule === 'thanks') {
        filename = 'default'
      }

      if (filename) {
        let homeIconElement = document.querySelectorAll('.js-home-icon')
        let homeIconArray = Array.prototype.slice.call(homeIconElement)
        
        homeIconArray.forEach(el => {
          el.setAttribute('href', `./img/daily/icon/${filename}.png${getQuery()}`)
        })
      }
    }

    if (screen.status === 'dailyJSONLoaded' && nextProps.screen.status === 'specialJSONLoaded') {
      actions.setStatus({
        status: 'allLoaded'
      })

      if (screen.schedule === 'daily') {
        actions.castSpecial();
      } else if (screen.schedule === 'history') {
        actions.castRandom();
      }
    }

    if (screen.status !== 'allLoaded' && nextProps.screen.status === 'allLoaded') {
      document.body.dataset.status = 'allLoaded'

      if (nextProps.screen.schedule === 'thanks') {
        let homeIconElement = document.querySelectorAll('.js-home-icon')
        let homeIconArray = Array.prototype.slice.call(homeIconElement)
        
        homeIconArray.forEach(el => {
          el.setAttribute('href', `./img/daily/icon/default.png${getQuery()}`)
        })
      }
    }

    if (screen.schedule === '' && nextScreen.schedule !== '') {
    }
  }

  render() {
    const { screen } = this.props
    const debugElement = screen.isDebug ?
      <Debug {...this.props} />
      : null
    const wrapperClass = classNames('wrapper', `device--${screen.device}`, {
      'schedule--history': screen.today.date >= screen.dreamlifeEndAt && screen.today.date < screen.lastDate,
      'schedule--thanks': screen.today.date >= screen.lastDate
    })

    return (
      <article className={wrapperClass}>
        <div className="fixed-bg fixed-bg--left" ref="fixed_bg_left"></div>
        <div className="fixed-bg fixed-bg--right" ref="fixed_bg_right"></div>
        <Header />
        <main className="main">
          <Navigation {...this.props} />
          <NormalPage {...this.props} call={this.refs.call} />
          <HistoryPage {...this.props} call={this.refs.call} />
          <Footer />
        </main>
        <Modal {...this.props} call={this.refs.call} />
        <audio src="./audio/call.mp3" ref="call" loop="true" />
        {debugElement}
      </article>
    )
  }
}

export default connect(
  state => ({screen: state.screen.toJS()}),
  dispatch => ({actions: bindActionCreators(actions, dispatch)})
)(App)
