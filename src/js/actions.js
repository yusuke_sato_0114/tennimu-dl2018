import { ActionTypes } from './constants'
import axios from 'axios'
import { getQuery } from './util'

export default {
  setWindowSize: data => ({ type: ActionTypes.WINDOW_SIZE_SET, data }),
  modalShow: data => ({ type: ActionTypes.MODAL_SHOW, data }),
  modalHide: () => ({ type: ActionTypes.MODAL_HIDE }),
  phoneIncoming: () => ({ type: ActionTypes.PHONE_INCOMING }),
  phoneLoading: () => ({type: ActionTypes.PHONE_LOADING }),
  phoneSpeaking: () => ({ type: ActionTypes.PHONE_SPEAKING }),
  phoneStop: () => ({ type: ActionTypes.PHONE_STOP }),
  setDevice: data => ({ type: ActionTypes.DEVICE_SET, data }),
  castSpecial: () => ({ type: ActionTypes.SPECIAL_CAST }),
  castRandom: () => ({ type: ActionTypes.RANDOM_CAST}),
  setDate: data => ({ type: ActionTypes.DATE_SET, data }),
  historyShow: () => ({ type: ActionTypes.HISTORY_SHOW }),
  historyHide: () => ({ type: ActionTypes.HISTORY_HIDE }),
  setSchedule: data => ({ type: ActionTypes.SCHEDULE_SET, data }),
  setStatus: data => ({ type: ActionTypes.STATUS_SET, data}),
  getDate: data => {
    return dispatch => {
      let date;

      if(data) { // for debug
        date = new Date(`${data.year}/${data.month}/${data.day} 12:00:00`);

        if(date.toString() === 'Invalid Date') {
          date = new Date();
        }

        dispatch({
          type: ActionTypes.DATE_GET,
          data: { date }
        });
      } else { // default
        axios.get(`${location.href}${getQuery()}`, {
          headers: {
            'Pragma': 'no-cache',
            'Cache-Control': 'no-cache'
          }
        })
          .then(res => {
            date = res.headers && res.headers.date ? new Date(res.headers.date) : new Date()

            dispatch({
              type: ActionTypes.DATE_GET,
              data: { date }
            });
          })
          .catch(() => {
            date = new Date();

            dispatch({
              type: ActionTypes.DATE_GET,
              data: { date }
            });
          })
      }
    };
  },
  getTodayJSON: encrypted => {
    return dispatch => {
      axios.get(`./json/${encrypted}.json${getQuery()}`)
        .then(({data}) => {
          dispatch({
            type: ActionTypes.TODAY_JSON_GET,
            data
          });
        })
    }
  },
  getSpecialJSON: () => {
    return dispatch => {
      axios.get(`./json/special.json${getQuery()}`)
        .then(({data}) => {
          dispatch({
            type: ActionTypes.SPECIAL_JSON_GET,
            data
          });
        })
    }
  }
}
