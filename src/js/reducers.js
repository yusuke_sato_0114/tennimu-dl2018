import { combineReducers } from 'redux'
import { Record, Map, List } from 'immutable'
import { ActionTypes } from './constants'
import _ from 'lodash'
import CryptoJS from 'crypto-js'

const cryptoKey = '42c79f423bb02d66367fa6fc710bf8bb000be8a4f3e51702452dd254c7ed4bd2b8b098e86a5dc1384a7a6b5170afcdd19abfeaad0ea2e2c4e8f5a49c0ce8add2'
const isDebug = location.host !== 'dl2018.tennimu.com'
const ScreenRecord = Record({
  windowSize: Map({
    width: 0,
    height: 0
  }),
  displayModal: false,
  modalType: null, // daily, history, random
  phoneState: 0, // 0: 着信中, 1: ロード中, 2: 通話中, 3: 通話終了
  cryptoKey,
  isDebug,
  today: Map({
    date: null,
    year: 0,
    month: 0,
    day: 0
  }),
  filename: '',
  role: '',
  name: '',
  schedule: '',
  status: 'initial', // initial, dateLoaded, dailyJSONLoaded, specialJSONLoaded, allLoaded
  startDate:      new Date('2018/04/04 12:00:00'),
  liveStartAt:    new Date('2018/05/05 12:00:00'),
  dreamlifeEndAt: new Date('2018/05/06 12:00:00'),
  lastDate:       new Date('2018/05/21 12:00:00'),
  device: null, // sp, pc
  special: Map({
    rate: isDebug ? 0.5 : 0.02,
    list: List([]),
    count: 0,
    castStartCount: 3,
    selected: Map({
      role: '',
      name: '',
      filename: ''
    })
  }),
  history: Map({
    display: false,
    list: List([]),
    selected: Map({
      role: '',
      name: '',
      filename: ''
    })
  }),
})

class Screen extends ScreenRecord {}

const parse = date => {
  // 正午前だったら前日
  if(date.getHours() < 12) {
    date = new Date(date.getTime() - (1000 * 60 * 60 * 24))
  }

  let parsed = {
    year:   date.getFullYear().toString(),
    month:  zero_padding(date.getMonth() + 1),
    day:    zero_padding(date.getDate())
  }

  parsed.date = new Date(`${parsed.year}/${parsed.month}/${parsed.day} 12:00:00`);
  return parsed
}
const decrypt = (str) => {
  return CryptoJS.AES.decrypt(str, cryptoKey).toString(CryptoJS.enc.Utf8)
}
const zero_padding = num => ('0' + num.toString()).slice(-2)
const screenReducer = (state = new Screen, action) => {
  const { type, data } = action

  switch (type) {
  case ActionTypes.WINDOW_SIZE_SET:
    return state.set('windowSize', data)
  case ActionTypes.MODAL_SHOW:
    switch (data.modalType) {
    case 'daily':
    case 'random':
      return state
        .set('displayModal', true)
        .set('modalType', data.modalType)
    case 'history':
      let { history } = data

      return state
        .set('displayModal', true)
        .set('modalType', data.modalType)
        .setIn(['history','selected','role'], history.role)
        .setIn(['history','selected','name'], history.name)
        .setIn(['history','selected','filename'], history.filename)
    default:
      return state
    }
  case ActionTypes.MODAL_HIDE:
    return state.set('displayModal', false)
  case ActionTypes.PHONE_INCOMING:
    return state.set('phoneState', 0)
  case ActionTypes.PHONE_LOADING:
    return state.set('phoneState', 1)
  case ActionTypes.PHONE_SPEAKING:
    return state.set('phoneState', 2)
  case ActionTypes.PHONE_STOP:
    return state.set('phoneState', 3)
  case ActionTypes.DATE_SET:
    return state.setIn(['today', data.name], data.value)
  case ActionTypes.DATE_GET:
    let parsed = parse(data.date)

    if(parsed.date < state.get('startDate')) {
      parsed = parse(state.get('startDate'));
    }

    if(parsed.date > state.get('lastDate')) {
      parsed = parse(state.get('lastDate'));
    }

    return state
      .setIn(['today','date'], parsed.date)
      .setIn(['today','year'], parsed.year)
      .setIn(['today','month'], parsed.month)
      .setIn(['today','day'], parsed.day)
      .set('status', 'dateLoaded')
  case ActionTypes.TODAY_JSON_GET:
    return state
      .set('filename', data.daily.filename)
      .set('role', data.daily.role)
      .set('name', data.daily.name)
      .setIn(['history', 'list'], data.history)
      .set('status', 'dailyJSONLoaded')
  case ActionTypes.SCHEDULE_SET:
    return state
      .set('schedule', data.schedule)
  case ActionTypes.STATUS_SET:
    return state
      .set('status', data.status)
  case ActionTypes.SPECIAL_JSON_GET:
    return state
      .setIn(['special','list'], data.list)
      .set('status', 'specialJSONLoaded')
  case ActionTypes.SPECIAL_CAST:
    if(state.getIn(['special', 'count']) < state.getIn(['special', 'castStartCount'])) {
      console.log(`still not cast. count: ${state.getIn(['special', 'count']) + 1}`)
      return state
        .setIn(['special', 'count'], state.getIn(['special', 'count']) + 1)
    }

    if(state.getIn(['special', 'rate']) > 0 && state.getIn(['special','list']).length > 0 && Math.random() < state.getIn(['special','rate'])) {
      let selected = _.sample(state.getIn(['special', 'list']))
      console.log('casted special');
      console.log(selected.index);
      return state
        .setIn(['special','selected','role'], decrypt(selected.role))
        .setIn(['special','selected','name'], decrypt(selected.name))
        .setIn(['special','selected','filename'], CryptoJS.HmacSHA1(`special_filename_${selected.index}`,cryptoKey))
    } else {
      console.log('miss casted')
      return state
        .setIn(['special','selected'], (new Screen).getIn(['special','selected']))
    }
  case ActionTypes.RANDOM_CAST:
    if(state.getIn(['special', 'rate']) > 0 && state.getIn(['special','list']).length > 0 && Math.random() < state.getIn(['special','rate'])) {
      let selected = _.sample(state.getIn(['special', 'list']))
      console.log('casted special');
      console.log(selected.index);
      return state
        .setIn(['special','selected','role'], decrypt(selected.role))
        .setIn(['special','selected','name'], decrypt(selected.name))
        .setIn(['special','selected','filename'], CryptoJS.HmacSHA1(`special_filename_${selected.index}`,cryptoKey))
    } else {
      let selected = _.sample(state.getIn(['history', 'list']))
      console.log('miss casted')
      return state
        .set('role', selected.role)
        .set('name', selected.name)
        .set('filename', selected.filename)
        .setIn(['special','selected'], (new Screen).getIn(['special','selected']))
    }
  case ActionTypes.HISTORY_SHOW:
    return state.setIn(['history','display'], true)
  case ActionTypes.HISTORY_HIDE:
    return state.setIn(['history','display'], false)
  case ActionTypes.DEVICE_SET:
    return state
      .set('device', data.device)
  default:
    return state
  }
}

export default combineReducers({
  screen: screenReducer
})
