export const getQuery = () => `?d=${(new Date).getTime()}`

const getCastNameText = data => {
  return data.role === '青学１年トリオ' ? data.role : `${data.role} ${data.name}`
}

export const getTweetText = {
  daily: data => {
    return `15周年記念コンサート Dream Live 2018 の特設サイトを公開中！特別企画 #ドリームライフ2 では出演キャストのビデオ通話風動画が日替わり更新！今日の担当は${getCastNameText(data)}`
  },
  dailySpecial: data => {
    return `15周年記念コンサート Dream Live 2018 の特設サイトを公開中！ 特別企画 #ドリームライフ2 では出演キャストのビデオ通話風動画が日替わり更新！${getCastNameText(data)}のレア動画を観ました！`
  },
  history: data => {
    return `15周年記念コンサート Dream Live 2018の開催を記念して、日替わり更新していた #ドリームライフ2 のビデオ通話風動画を特別限定公開中！${getCastNameText(data)}の着信履歴をチェックしました！`
  },
  random: data => getTweetText.history(data),
  randomSpecial: data => {
    return `15周年記念コンサート Dream Live 2018の開催を記念して、日替わり更新していた #ドリームライフ2 のビデオ通話風動画を特別限定公開中！${getCastNameText(data)}のレア動画を観ました！`
  }
}
