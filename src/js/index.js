import React from 'react'
import { render } from 'react-dom'
import { applyMiddleware, createStore } from 'redux'
import { Provider } from 'react-redux'
import App from './App'
import thunk from 'redux-thunk';
import rootReducer from './reducers'

const store = applyMiddleware(thunk)(createStore)(rootReducer)

render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('app')
)
