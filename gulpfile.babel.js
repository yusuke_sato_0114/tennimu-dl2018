import gulp from 'gulp'
import connect from 'connect'
import serveStatic from 'serve-static'
import livereload from 'connect-livereload'
import runSequence from 'run-sequence'
import cssnext from 'postcss-cssnext'
import pcImport from 'postcss-import'
import pcMixins from 'postcss-mixins'
import pcAssets from 'postcss-assets'
import pcAV from 'postcss-advanced-variables'
import cssnano from 'cssnano'

const $ = require('gulp-load-plugins')()
const errorHandler = err => {
  console.log(err.message)
}
const isProd = process.env.NODE_ENV === 'production'
const browsers = [
  'last 3 Firefox versions',
  'last 3 Chrome versions',
  'last 2 Safari versions',
  'ie >= 11',
  'iOS >= 10',
  'Android >= 4.0'
]
const dest = isProd ? 'dist' : 'public'

gulp.task('postcss', () => {
  gulp
    .src('./src/css/style.css')
    .pipe($.plumber({ errorHandler }))
    .pipe($.postcss([
      pcImport(),
      pcMixins({
        mixinsDir: `${__dirname}/src/css/mixins`
      }),
      pcAV(),
      pcAssets({
        loadPaths: ['img/'],
        basePath: './src',
        cachebuster: true,
        relative: '.'
      }),
      cssnext({ browsers }),
      ...(isProd ? [
        cssnano({ autoprefixer: false })
      ] : [])
    ]))
    .pipe(gulp.dest(dest))
    .pipe($.livereload())
});

gulp.task('serve', () => {
  $.livereload.listen()
  connect()
    .use(livereload())
    .use(serveStatic(dest))
    .listen(9000)
})

gulp.task('copy', () => {
  gulp.src(
    [
      'src/index.html',
      'src/json/**',
      'src/img/**',
      'src/audio/*',
      'src/favicon*',
      'src/video/**',
    ],
    { base: 'src'}
  )
  .pipe(gulp.dest(dest))
})

gulp.task('watch', () => {
  gulp.watch(['src/css/**/*'], ['postcss'])
})

gulp.task('build', ['postcss', 'copy'])
gulp.task('default', ['serve', 'build', 'watch'])